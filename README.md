# Simple mailer

## Ejemplo

``` curl
curl --request POST \
  --url http://localhost:3000/mailer \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --data '{\n   "text":    "i hope this works", \n   "from":    "you <username@outlook.com>", \n   "to":      "Joaquin <the.jogs.1917@gmail.com>",\n   "subject": "testing emailjs"\n}'
```
