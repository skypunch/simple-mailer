const PORT = process.env.port || 3000;
const express = require('express');
const bodyParser = require('body-parser');
const CONF = require('./conf');
const email = require('emailjs');

let server = email.server.connect(CONF);

let app = express();

app.use(bodyParser.json());

app.route('/mailer')
.post((req,res)=>{
  if(req.is('json')){
    console.log(req.body);
    server.send(req.body,(err, message)=>{
      if(err){
          res.status(500)
          .type('json')
          .json(err)
      }
      else{
        res.status(200)
        .type('json')
        .json(message);
      }
    });
  }
  else{
    res.status(500)
    .type('json')
    .json({
      "message":"Se esperaba que el cuerpo de la solicitud sea JSON"
    })
  }
})

app.listen(PORT,()=>{
  console.log('Aplicacion corriendo en '+PORT);
})
